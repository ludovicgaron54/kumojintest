package com.kumojin.events.controller;

import java.util.List;

import com.kumojin.events.model.Event;
import com.kumojin.events.model.ResponseEvent;
import com.kumojin.events.service.EventsService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequiredArgsConstructor
@RequestMapping("/api/")
public class EventsController {
    private final EventsService eventsService;

    private final ResponseEventValidator validator;

    @GetMapping(path = "/events")
    public ResponseEntity<List<Event>> listEvents() {
        List<Event> resources = eventsService.getEvents();
        return ResponseEntity.ok(resources);
    }
	
	@PostMapping(path = "/event")
	public ResponseEntity<Event> saveEvent(@RequestBody ResponseEvent responseEvent) {
        if (validator.validateResponseEvent(responseEvent)) {
            var event = new Event(responseEvent);
            Event resource = eventsService.saveEvent(event);
            return ResponseEntity.ok(resource);
        }
        return ResponseEntity.badRequest().build();
    }
}