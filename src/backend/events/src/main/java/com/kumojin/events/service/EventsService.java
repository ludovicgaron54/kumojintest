package com.kumojin.events.service;

import java.util.List;

import com.kumojin.events.model.Event;
import com.kumojin.events.EventsRepository;
import org.springframework.stereotype.Component;

@Component
public class EventsService {

    private final EventsRepository eventsRepository;

    public EventsService(EventsRepository eventsRepository) {
        this.eventsRepository = eventsRepository;
    }

    public List<Event> getEvents() {
        return eventsRepository.findAll();
    }
    
    public Event saveEvent(Event event) {
    	return eventsRepository.save(event);
    }
}
