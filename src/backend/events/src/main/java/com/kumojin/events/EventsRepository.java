package com.kumojin.events;

import org.springframework.data.jpa.repository.JpaRepository;

import com.kumojin.events.model.Event;

public interface EventsRepository extends JpaRepository<Event, Long> {}