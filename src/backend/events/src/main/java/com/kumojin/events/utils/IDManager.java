package com.kumojin.events.utils;

public class IDManager {
    private static long idCounter = 0;

    public static synchronized long createID()
    {
        return idCounter++;
    }
}
