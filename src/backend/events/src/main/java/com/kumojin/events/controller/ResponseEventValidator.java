package com.kumojin.events.controller;

import com.kumojin.events.model.ResponseEvent;
import com.kumojin.events.utils.DateUtils;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.time.format.DateTimeParseException;

@Component
public class ResponseEventValidator {
    public boolean validateResponseEvent(ResponseEvent responseEvent) {
        return ResponseEventValidator.validateName(responseEvent.name()) &&
                ResponseEventValidator.validateDescription(responseEvent.description()) &&
                ResponseEventValidator.validateDateTime(responseEvent.startDate(), responseEvent.endDate(),
                        responseEvent.startHours(), responseEvent.startMinutes(),
                        responseEvent.endHours(), responseEvent.endMinutes()) &&
                ResponseEventValidator.validateTimezone(responseEvent.timezone());
    }

    private static boolean validateName(String name) {
        return name.length() > 0 && name.length() <= 32;
    }

    private static boolean validateDescription(String description) {
        return description.length() > 0;
    }

    private static boolean validateDateTime(String startDate, String endDate,
                                           String startHours, String startMinutes, String endHours, String endMinutes) {
        if (validateDates(startDate, endDate) && validateTimes(startHours, startMinutes, endHours, endMinutes)) {
            if (DateUtils.stringToDate(startDate).isEqual(DateUtils.stringToDate(endDate))) {
                var startHour = Integer.parseInt(startHours);
                var endHour = Integer.parseInt(endHours);
                return startHour < endHour || (startHour == endHour && Integer.parseInt(startMinutes) < Integer.parseInt(endMinutes));
            }
            return true;
        }
        return false;
    }

    private static boolean validateDates(String startDate, String endDate) {
        LocalDate startLocalDate;
        LocalDate endLocalDate;

        try {
            startLocalDate = DateUtils.stringToDate(startDate);
            endLocalDate = DateUtils.stringToDate(endDate);
        } catch (DateTimeParseException e) {
            return false;
        }

        return startLocalDate.isBefore(endLocalDate) || startLocalDate.isEqual(endLocalDate);
    }

    private static boolean validateTimes(String startHours, String startMinutes, String endHours, String endMinutes) {
        return validateHoursMinutes(startHours, startMinutes) && validateHoursMinutes(endHours, endMinutes);
    }

    private static boolean validateTimezone(String timezone) {
        return timezone.length() > 2 && timezone.contains("/");
    }

    private static boolean validateHoursMinutes(String hours, String minutes) {
        return stringIsTimeNumber(hours, 23) && stringIsTimeNumber(minutes, 59);
    }

    private static boolean stringIsTimeNumber(String time, int maximum) {
        if (!isInteger(time)) {
            return false;
        }

        int i = Integer.parseInt(time);

        return i >= 0 && i <= maximum;
    }

    private static boolean isInteger(String strNum) {
        if (strNum == null) {
            return false;
        }
        try {
            Integer.parseInt(strNum);
        } catch (NumberFormatException nfe) {
            return false;
        }
        return true;
    }
}
