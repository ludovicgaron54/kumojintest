package com.kumojin.events.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;

import com.kumojin.events.utils.DateUtils;
import com.kumojin.events.utils.IDManager;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.time.LocalTime;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Event {
	@Id
	@Column
    private long id;

    @Column
    @NotNull(message="{NotNull.Event.name}")
    private String name;
    
    @Column
    @NotNull(message="{NotNull.Event.description}")
    private String description;
    
    @Column
    @NotNull(message="{NotNull.Event.startDate}")
    private LocalDateTime startDate;

    @Column
    @NotNull(message="{NotNull.Event.endDate}")
    private LocalDateTime endDate;

    @Column
    @NotNull(message="{NotNull.Event.timezone}")
    private String timezone;

    public Event(ResponseEvent responseEvent) {
        this.id = IDManager.createID();

        name = responseEvent.name();
        description = responseEvent.description();
        startDate = mergeDateTime(responseEvent.startDate(), responseEvent.startHours(), responseEvent.startMinutes());
        endDate = mergeDateTime(responseEvent.endDate(), responseEvent.endHours(), responseEvent.endMinutes());
        timezone = responseEvent.timezone();
    }

    private LocalDateTime mergeDateTime(String date, String hours, String minutes) {
        return LocalDateTime.of(DateUtils.stringToDate(date), LocalTime.of(Integer.parseInt(hours), Integer.parseInt(minutes)));
    }
}