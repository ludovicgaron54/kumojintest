package com.kumojin.events.model;

public record ResponseEvent(String name,
                            String description,
                            String startDate,
                            String endDate,
                            String startHours,
                            String startMinutes,
                            String endHours,
                            String endMinutes,
                            String timezone) {
    public ResponseEvent withName(String name) {
        return new ResponseEvent(name, description, startDate, endDate, startHours, startMinutes, endHours, endMinutes, timezone);
    }

    public ResponseEvent withDescription(String description) {
        return new ResponseEvent(name, description, startDate, endDate, startHours, startMinutes, endHours, endMinutes, timezone);
    }

    public ResponseEvent withStartDate(String startDate) {
        return new ResponseEvent(name, description, startDate, endDate, startHours, startMinutes, endHours, endMinutes, timezone);
    }

    public ResponseEvent withEndDate(String endDate) {
        return new ResponseEvent(name, description, startDate, endDate, startHours, startMinutes, endHours, endMinutes, timezone);
    }

    public ResponseEvent withStartHours(String startHours) {
        return new ResponseEvent(name, description, startDate, endDate, startHours, startMinutes, endHours, endMinutes, timezone);
    }

    public ResponseEvent withStartMinutes(String startMinutes) {
        return new ResponseEvent(name, description, startDate, endDate, startHours, startMinutes, endHours, endMinutes, timezone);
    }

    public ResponseEvent withEndHours(String endHours) {
        return new ResponseEvent(name, description, startDate, endDate, startHours, startMinutes, endHours, endMinutes, timezone);
    }

    public ResponseEvent withEndMinutes(String endMinutes) {
        return new ResponseEvent(name, description, startDate, endDate, startHours, startMinutes, endHours, endMinutes, timezone);
    }

    public ResponseEvent withTimezone(String timezone) {
        return new ResponseEvent(name, description, startDate, endDate, startHours, startMinutes, endHours, endMinutes, timezone);
    }
}
