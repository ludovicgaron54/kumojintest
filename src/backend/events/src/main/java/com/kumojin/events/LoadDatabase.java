package com.kumojin.events;

import com.kumojin.events.model.Event;
import com.kumojin.events.utils.IDManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.time.LocalDateTime;

@Configuration
class LoadDatabase {

    private static final Logger log = LoggerFactory.getLogger(LoadDatabase.class);

    @Bean
    CommandLineRunner initDatabase(EventsRepository repository) {
        return args -> {
            log.info("Preloading " + repository.save(new Event(IDManager.createID(), "Event 1", "first event", LocalDateTime.of(1970, 12, 5, 17, 59), LocalDateTime.of(1970, 12, 6, 10, 15), "America/Chicago")));
            log.info("Preloading " + repository.save(new Event(IDManager.createID(), "Event 2", "second event", LocalDateTime.of(1971, 2, 10, 1, 2), LocalDateTime.of(1973, 1, 6, 0, 0), "America/Chicago")));
        };
    }
}
