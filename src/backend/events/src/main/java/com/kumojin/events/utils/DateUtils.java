package com.kumojin.events.utils;

import java.time.LocalDate;

public class DateUtils {
    public static LocalDate stringToDate(String date) {
        return LocalDate.parse(date.split("T")[0]);
    }
}
