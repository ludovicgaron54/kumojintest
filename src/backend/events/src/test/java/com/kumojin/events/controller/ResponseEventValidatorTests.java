package com.kumojin.events.controller;

import com.kumojin.events.model.ResponseEvent;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import static org.springframework.test.util.AssertionErrors.*;

@SpringBootTest
public class ResponseEventValidatorTests {
    private final String EMPTY = "";
    private final String TOO_LONG = "1234567890123456789012345678901234567890";
    private final String VALID_NAME = "name";
    private final String VALID_DESCRIPTION = "description";
    private final String VALID_START_DATE = "2022-12-25";
    private final String VALID_END_DATE = "2022-12-26";
    private final String VALID_START_HOURS = "8";
    private final String VALID_START_MINUTES = "0";
    private final String VALID_END_HOURS = "11";
    private final String VALID_END_MINUTES = "59";
    private final String VALID_TIMEZONE = "America/Montreal";

    private final ResponseEvent VALID_RESPONSE_EVENT = new ResponseEvent(
            VALID_NAME,
            VALID_DESCRIPTION,
            VALID_START_DATE,
            VALID_END_DATE,
            VALID_START_HOURS,
            VALID_START_MINUTES,
            VALID_END_HOURS,
            VALID_END_MINUTES,
            VALID_TIMEZONE);

    private ResponseEventValidator responseEventValidator;

    @BeforeEach
    public void setup() {
        responseEventValidator = new ResponseEventValidator();
    }

    @Test
    void validateValidResponseEvent() {
        assertTrue("Should return true when given a valid ResponseEvent",
                responseEventValidator.validateResponseEvent(VALID_RESPONSE_EVENT));
        assertTrue("Should return true when given a valid ResponseEvent",
                responseEventValidator.validateResponseEvent(VALID_RESPONSE_EVENT.withEndDate(VALID_START_DATE)));
        assertTrue("Should return true when given a ResponseEvent with an end minute later in the day than the start",
                responseEventValidator.validateResponseEvent(VALID_RESPONSE_EVENT
                        .withEndDate(VALID_START_DATE).withEndHours(VALID_START_HOURS)
                        .withEndMinutes("1")));

    }

    @Test
    void validateInvalidNameResponseEvent() {
        assertFalse("Should return false when given a ResponseEvent with an empty name",
                responseEventValidator.validateResponseEvent(VALID_RESPONSE_EVENT.withName(EMPTY)));

        assertFalse("Should return false when given a ResponseEvent with a name that's too long",
                responseEventValidator.validateResponseEvent(VALID_RESPONSE_EVENT.withName(TOO_LONG)));
    }

    @Test
    void validateInvalidDescriptionResponseEvent() {
        assertFalse("Should return false when given a ResponseEvent with an invalid description",
                responseEventValidator.validateResponseEvent(VALID_RESPONSE_EVENT.withDescription(EMPTY)));
    }

    @Test
    void validateInvalidDateTimeResponseEvent() {
        assertFalse("Should return false when given a ResponseEvent with an invalid start date",
                responseEventValidator.validateResponseEvent(VALID_RESPONSE_EVENT.withStartDate(EMPTY)));

        assertFalse("Should return false when given a ResponseEvent with an invalid end date",
                responseEventValidator.validateResponseEvent(VALID_RESPONSE_EVENT.withEndDate(EMPTY)));

        assertFalse("Should return false when given a ResponseEvent with an end date before start date",
                responseEventValidator.validateResponseEvent(VALID_RESPONSE_EVENT
                        .withEndDate(VALID_START_DATE).withStartDate(VALID_END_DATE)));


        validateTime(VALID_RESPONSE_EVENT.withStartHours(EMPTY),
                VALID_RESPONSE_EVENT.withStartHours("a"),
                VALID_RESPONSE_EVENT.withStartHours("-1"),
                VALID_RESPONSE_EVENT.withStartHours("24"),
                "start hour");

        validateTime(VALID_RESPONSE_EVENT.withStartMinutes(EMPTY),
                VALID_RESPONSE_EVENT.withStartMinutes("a"),
                VALID_RESPONSE_EVENT.withStartMinutes("-1"),
                VALID_RESPONSE_EVENT.withStartMinutes("60"),
                "start minute");

        validateTime(VALID_RESPONSE_EVENT.withEndHours(EMPTY),
                VALID_RESPONSE_EVENT.withEndHours("a"),
                VALID_RESPONSE_EVENT.withEndHours("-1"),
                VALID_RESPONSE_EVENT.withEndHours("24"),
                "end hour");

        validateTime(VALID_RESPONSE_EVENT.withEndMinutes(EMPTY),
                VALID_RESPONSE_EVENT.withEndMinutes("a"),
                VALID_RESPONSE_EVENT.withEndMinutes("-1"),
                VALID_RESPONSE_EVENT.withEndMinutes("60"),
                "end hour");


        assertFalse("Should return false when given a ResponseEvent with an end hour sooner in the day than the start",
                responseEventValidator.validateResponseEvent(VALID_RESPONSE_EVENT
                        .withEndDate(VALID_START_DATE).withEndHours("7")));

        assertFalse("Should return false when given a ResponseEvent with an end minute sooner in the day than the start",
                responseEventValidator.validateResponseEvent(VALID_RESPONSE_EVENT
                        .withEndDate(VALID_START_DATE).withEndHours(VALID_START_HOURS)
                        .withEndMinutes("30").withStartMinutes("31")));
    }

    @Test
    void validateInvalidTimezoneResponseEvent() {
        assertFalse("Should return false when given a ResponseEvent with an empty timezone",
                responseEventValidator.validateResponseEvent(VALID_RESPONSE_EVENT.withTimezone(EMPTY)));

        assertFalse("Should return false when given a ResponseEvent with a too short timezone",
                responseEventValidator.validateResponseEvent(VALID_RESPONSE_EVENT.withTimezone("A/")));

        assertFalse("Should return false when given a ResponseEvent with a timezone that doesn't have a /",
                responseEventValidator.validateResponseEvent(VALID_RESPONSE_EVENT.withTimezone("America-Montreal")));
    }

    void validateTime(ResponseEvent emptyResponseEvent,
                      ResponseEvent notNumberResponseEvent,
                      ResponseEvent negativeResponseEvent,
                      ResponseEvent tooHighResponseEvent,
                      String testDescription) {
        assertFalse("Should return false when given a ResponseEvent with an empty " + testDescription,
                responseEventValidator.validateResponseEvent(emptyResponseEvent));

        assertFalse("Should return false when given a ResponseEvent with not a number " + testDescription,
                responseEventValidator.validateResponseEvent(notNumberResponseEvent));

        assertFalse("Should return false when given a ResponseEvent with a negative " + testDescription,
                responseEventValidator.validateResponseEvent(negativeResponseEvent));

        assertFalse("Should return false when given a ResponseEvent with a too high " + testDescription,
                responseEventValidator.validateResponseEvent(tooHighResponseEvent));
    }
}
