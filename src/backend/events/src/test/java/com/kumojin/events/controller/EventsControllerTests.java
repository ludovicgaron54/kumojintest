package com.kumojin.events.controller;

import com.kumojin.events.model.Event;
import com.kumojin.events.model.ResponseEvent;
import com.kumojin.events.service.EventsService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.util.AssertionErrors.*;

@SpringBootTest
public class EventsControllerTests {
    private final Event EVENT_1 = new Event(1, "1", "first event",
            LocalDateTime.of(2022, 8, 11, 4, 30),
            LocalDateTime.of(2022, 8, 11, 5, 30),
            "America/Montreal");

    private final Event EVENT_2 = new Event(2, "2", "second event",
            LocalDateTime.of(2022, 7, 11, 4, 30),
            LocalDateTime.of(2022, 9, 3, 5, 25),
            "America/Montreal");

    private final ResponseEvent RESPONSE_EVENT_1 = new ResponseEvent(EVENT_1.getName(), EVENT_1.getDescription(),
            EVENT_1.getStartDate().toLocalDate().toString(), EVENT_1.getEndDate().toLocalDate().toString(),
            String.valueOf(EVENT_1.getStartDate().getHour()), String.valueOf(EVENT_1.getStartDate().getMinute()),
            String.valueOf(EVENT_1.getEndDate().getHour()), String.valueOf(EVENT_1.getEndDate().getMinute()),
            EVENT_1.getTimezone());

    @Mock
    private EventsService eventsServiceMock;

    @Mock
    private ResponseEventValidator validator;

    private EventsController eventsController;

    @BeforeEach
    public void setup() {
        eventsController = new EventsController(eventsServiceMock, validator);
    }

    @Test
    public void testFilledListEvents() {
        when(eventsServiceMock.getEvents()).thenReturn(List.of(EVENT_1, EVENT_2));

        var listingEvents = eventsController.listEvents();

        assertEquals("Should return an OK response", HttpStatus.OK, listingEvents.getStatusCode());
        assertEquals("Should return a list with the 2 events in the body", 2, Objects.requireNonNull(listingEvents.getBody()).size());
        assertTrue("Should contain the first event", listingEvents.getBody().contains(EVENT_1));
        assertTrue("Should contain the second event", listingEvents.getBody().contains(EVENT_2));
    }

    @Test
    public void testEmptyListEvents() {
        when(eventsServiceMock.getEvents()).thenReturn(List.of());

        var listingEvents = eventsController.listEvents();

        assertEquals("Should return an OK response", HttpStatus.OK, listingEvents.getStatusCode());
        assertEquals("Should return an empty list", 0, Objects.requireNonNull(listingEvents.getBody()).size());
        assertFalse("Should not contain the first event", listingEvents.getBody().contains(EVENT_1));
        assertFalse("Should not contain the second event", listingEvents.getBody().contains(EVENT_2));
    }

    @Test
    public void testSaveValidEvent() {
        when(validator.validateResponseEvent(any())).thenReturn(true);
        when(eventsServiceMock.saveEvent(any())).thenReturn(EVENT_1);

        var response = eventsController.saveEvent(RESPONSE_EVENT_1);
        assertEquals("Should return an OK response", HttpStatus.OK, response.getStatusCode());
        assertEquals("Should return the converted event", EVENT_1, response.getBody());
    }

    @Test
    public void testSaveInvalidEvent() {
        when(validator.validateResponseEvent(any())).thenReturn(false);
        when(eventsServiceMock.saveEvent(any())).thenReturn(EVENT_1);

        var response = eventsController.saveEvent(RESPONSE_EVENT_1);
        assertEquals("Should return a bad request response", HttpStatus.BAD_REQUEST, response.getStatusCode());
        assertEquals("Should return nothing", null, response.getBody());
    }
}
