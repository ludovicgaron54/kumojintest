export class Event {

  constructor(
    public name: string,
    public description: string,
    public startDate: Date,
    public endDate: Date,
    public startHours: string,
    public startMinutes: string,
    public endHours: string,
    public endMinutes: string,
    public timezone: string
  ) { }
}
