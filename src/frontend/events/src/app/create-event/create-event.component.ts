import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {AppService} from "../app.service";
import {lookupViaCity, cityMapping} from 'city-timezones';
import {map, Observable, startWith} from "rxjs";
import {MatSnackBar} from "@angular/material/snack-bar";
@Component({
  selector: 'app-create-event',
  templateUrl: './create-event.component.html',
  styleUrls: ['./create-event.component.css']
})
export class CreateEventComponent implements OnInit {

  submitted = false;

  eventForm = new FormGroup({
    name: new FormControl('', [Validators.required, Validators.maxLength(32), Validators.minLength(1), Validators.nullValidator]),
    description: new FormControl('', [Validators.required, Validators.minLength(1), Validators.nullValidator]),
    startDate: new FormControl<Date | undefined>(undefined, [Validators.required, Validators.nullValidator]),
    endDate: new FormControl<Date | undefined>(undefined, [Validators.required, Validators.nullValidator]),
    startHours: new FormControl(''),
    startMinutes: new FormControl(''),
    endHours: new FormControl(''),
    endMinutes: new FormControl(''),
    city: new FormControl(''),
    timezone: new FormControl('', [Validators.required, Validators.minLength(1), Validators.nullValidator]),
  });

  cityFound = { name: '', timezone: '' };
  cityOptions: string[] = cityMapping.filter(city => city.timezone != null).map(city => city.city);
  cityFilteredOptions: Observable<string[]> | undefined;

  timezoneOptions: string[] = [...new Set(cityMapping.filter(city => city.timezone != null).map(city => city.timezone))];
  timezoneFilteredOptions: Observable<string[]> | undefined;

  constructor(private appService: AppService, private eventSnackBar: MatSnackBar) { }

  ngOnInit() {
    this.eventForm.controls.city.registerOnChange(this.onCityChange);
    this.cityFilteredOptions = this.eventForm.controls.city.valueChanges.pipe(
      startWith(''),
      map(value => this.cityFilter(value || '')),
    );

    this.timezoneFilteredOptions = this.eventForm.controls.timezone.valueChanges.pipe(
      startWith(''),
      map(value => this.timezoneFilter(value || '')),
    );
  }

  private cityFilter(value: string): string[] {
    if (value.length < 3) return [];
    const filterValue = value.toLowerCase();

    return this.cityOptions.filter(option => option.toLowerCase().includes(filterValue));
  }

  private timezoneFilter(value: string): string[] {
    if (value.length < 3) return [];
    const filterValue = value.toLowerCase();

    return this.timezoneOptions.filter(option => option.toLowerCase().includes(filterValue));
  }

  onCityChange() {
    console.log('change');
    let city = this.eventForm.value.city;
    if (city) {
      let guesses = lookupViaCity(city);
      if (guesses.length > 0) {
        this.cityFound.name = guesses[0].city;
        this.cityFound.timezone = guesses[0].timezone;
      }
    }
  }

  applyTimezone() {
    this.eventForm.controls.timezone.setValue(this.cityFound.timezone);
  }

  onSubmit() {
    let name = this.eventForm.value.name;
    let description = this.eventForm.value.description;
    let startDate = this.eventForm.value.startDate;
    let endDate = this.eventForm.value.endDate;
    let startHours = this.eventForm.value.startHours;
    let startMinutes = this.eventForm.value.startMinutes;
    let endHours = this.eventForm.value.endHours;
    let endMinutes = this.eventForm.value.endMinutes;
    let timezone = this.eventForm.value.timezone;

    if(name && description && startDate && endDate && startHours && startMinutes && endHours && endMinutes && timezone) {
      this.appService.addEvent({ name, description, startDate, endDate, startHours, startMinutes, endHours, endMinutes, timezone })
        .subscribe({
          next: () => {
            this.eventSnackBar.open('Event Created!', 'Sweet');
          },
          error: (error) => {
            console.log(error);
            this.eventSnackBar.open('Could not create event', 'Modify event');
          }
      });
    }
    else {
      this.eventSnackBar.open('There are incomplete fields');
    }
  }
}
