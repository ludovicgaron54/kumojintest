import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateEventComponent } from './create-event.component';
import {HttpClientModule} from "@angular/common/http";
import {MatSnackBarModule} from "@angular/material/snack-bar";
import {MatAutocompleteModule} from "@angular/material/autocomplete";

describe('CreateEventComponent', () => {
  let component: CreateEventComponent;
  let fixture: ComponentFixture<CreateEventComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [HttpClientModule, MatSnackBarModule, MatAutocompleteModule],
      declarations: [ CreateEventComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(CreateEventComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

describe('onSubmit', () => {
  let component: CreateEventComponent;
  let appServiceSpy: any;
  let responseSpy: any;
  let eventSnackBarSpy: any;

  beforeEach(() => {
    appServiceSpy = jasmine.createSpyObj(['addEvent']);
    responseSpy = jasmine.createSpyObj(['subscribe']);
    appServiceSpy.addEvent.and.returnValue(responseSpy);
    eventSnackBarSpy = jasmine.createSpyObj(['open']);
    component = new CreateEventComponent(appServiceSpy, eventSnackBarSpy);
    component.eventForm.value.name = 'name';
    component.eventForm.value.description = 'description';
    component.eventForm.value.startDate = new Date();
    component.eventForm.value.endDate = new Date();
    component.eventForm.value.startHours = '1';
    component.eventForm.value.startMinutes = '2';
    component.eventForm.value.endHours = '3';
    component.eventForm.value.endMinutes = '4';
    component.eventForm.value.timezone = 'timezone';
  });

  describe('when all fields are filled', () => {
    it('should call addEvent', () => {
      component.onSubmit();
      expect(appServiceSpy.addEvent).toHaveBeenCalledTimes(1);
      expect(responseSpy.subscribe).toHaveBeenCalledTimes(1);
    });
  });

  describe('when name is missing', () => {
    it('should not call addEvent, should call open with \'There are incomplete fields\'', () => {
      component.eventForm.value.name = '';
      incompleteFieldsTests();
    });
  });

  describe('when startDate is missing', () => {
    it('should not call addEvent, should call open with \'There are incomplete fields\'', () => {
      component.eventForm.value.startDate = null;
      incompleteFieldsTests();
    });
  });

  describe('when endDate is missing', () => {
    it('should not call addEvent, should call open with \'There are incomplete fields\'', () => {
      component.eventForm.value.endDate = null;
      incompleteFieldsTests();
    });
  });

  describe('when startHours is missing', () => {
    it('should not call addEvent, should call open with \'There are incomplete fields\'', () => {
      component.eventForm.value.startHours = '';
      incompleteFieldsTests();
    });
  });

  describe('when startMinutes is missing', () => {
    it('should not call addEvent, should call open with \'There are incomplete fields\'', () => {
      component.eventForm.value.startMinutes = '';
      incompleteFieldsTests();
    });
  });

  describe('when endHours is missing', () => {
    it('should not call addEvent, should call open with \'There are incomplete fields\'', () => {
      component.eventForm.value.endHours = '';
      incompleteFieldsTests();
    });
  });

  describe('when endMinutes is missing', () => {
    it('should not call addEvent, should call open with \'There are incomplete fields\'', () => {
      component.eventForm.value.endMinutes = '';
      incompleteFieldsTests();
    });
  });

  describe('when timezone is missing', () => {
    it('should not call addEvent, should call open with \'There are incomplete fields\'', () => {
      component.eventForm.value.timezone = '';
      incompleteFieldsTests();
    });
  });

  const incompleteFieldsTests = () => {
    component.onSubmit();

    expect(appServiceSpy.addEvent).toHaveBeenCalledTimes(0);
    expect(responseSpy.subscribe).toHaveBeenCalledTimes(0);
    expect(eventSnackBarSpy.open).toHaveBeenCalledOnceWith('There are incomplete fields');
  }
});
