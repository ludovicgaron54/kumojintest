import {Component, Input, OnInit} from '@angular/core';
import {FormControl, Validators} from "@angular/forms";

@Component({
  selector: 'app-time',
  templateUrl: './time.component.html',
  styleUrls: ['./time.component.css']
})
export class TimeComponent implements OnInit {
  @Input()
  hourControl: FormControl = new FormControl();

  @Input()
  minuteControl: FormControl = new FormControl();

  @Input()
  distinctName: string = '';

  constructor() {
  }

  ngOnInit(): void {
    this.hourControl.setValidators([Validators.required,
      Validators.minLength(1),
      Validators.maxLength(2),
      Validators.nullValidator,
      Validators.min(0),
      Validators.max(23),
      Validators.pattern("^[0-9]{1,2}$")]);

    this.minuteControl.setValidators(
      [Validators.required,
        Validators.minLength(1),
        Validators.maxLength(2),
        Validators.nullValidator,
        Validators.min(0),
        Validators.max(59),
        Validators.pattern("^[0-9]{1,2}$")]);
  }

}
