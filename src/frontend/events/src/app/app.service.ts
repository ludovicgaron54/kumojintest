import { HttpClient } from "@angular/common/http";
import {Event} from "./event";
import {Injectable} from "@angular/core";

const apiPath = '/api/';

@Injectable({
  providedIn: 'root'
})
export class AppService {
  constructor(private httpClient: HttpClient) {  }

  getEvents() {
    return this.httpClient.get(apiPath + 'events');
  }

  addEvent(event: Event) {
    return this.httpClient.post(apiPath + 'event', event);
  }
}
